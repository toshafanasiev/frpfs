﻿open System
open System.Drawing
open System.Windows.Forms
open Time

type Ball = { r : double; x : double; y : double; vx : double; vy : double }

let abs v = if v < 0.0 then -v else v

let bounceMin bound d v =
    if d < bound then abs v else v

let bounceMax bound d v =
    if d > bound then -abs v else v

let contain (r : Rectangle) (b : Ball) : Ball =
    let vx' = bounceMin (double r.Left) b.x b.vx
    let vx'' = bounceMax (double r.Right) b.x vx'
    let vy' = bounceMin (double r.Top) b.y b.vy
    let vy'' = bounceMax (double r.Bottom) b.y vy'
    { b with vx = vx''; vy = vy'' }

let move (b : Ball) (ts : TimeSpan) : Ball =
    let dt = ts.TotalSeconds
    in { b with x = b.x + b.vx * dt; y = b.y + b.vy * dt }

let acc a v (ts : TimeSpan) : double =
    v + a * ts.TotalSeconds

let gravity (b : Ball) (ts : TimeSpan) : Ball =
    { b with vy = acc 981.0 b.vy ts }

let render (g : Graphics) (b : Ball) : unit =
    let brush = new SolidBrush(Color.Red)
    g.Clear (Color.White)
    g.FillEllipse (brush, (float32 (b.x - b.r)), (float32 (b.y - b.r)), 2.0f * (float32 b.r), 2.0f *(float32 b.r))

[<EntryPoint>]
let main argv = 
    let f = new Form (Width = 1500, Height = 900)
    let update b ts =
        let b' = gravity b ts
        let b'' = move b' ts
        in contain f.DisplayRectangle b''
    let ball = { r = 50.0; x = 5.0; y = 5.0; vx = 500.0; vy = 500.0 }
    let bsig = Observable.map (foldp update ball) (fps 60)
    use g = BufferedGraphicsManager.Current.Allocate (f.CreateGraphics(), f.DisplayRectangle)
    use signal = bsig |> Observable.subscribe (fun b -> render g.Graphics b; g.Render())
    Application.Run f
    0 
