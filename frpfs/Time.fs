﻿module Time

open System
open System.Windows.Forms

let time r =
    let timer = new Timer ()
    timer.Interval <- 1000 / r
    timer.Start ()
    timer.Tick |> Observable.map (fun _ -> DateTime.Now)

let foldp f i =
    let s = ref i
    let update n =
        let r = f !s n
        s := r
        r
    update

let last2 (p,c) n = (c,n)

let init v = (v,v)

let delta (t1,t2) = t2 - t1

let fps n =
    let i = init DateTime.Now
    let d = (foldp last2 i) >> delta
    let t = time n
    Observable.map d t
